# Linux Server Hardening
#### Sources
- **Linux Hardening Guide** : https://madaidans-insecurities.github.io/guides/linux-hardening.html
- **Recommandations de configuration d'un système GNU/Linux** : https://www.ssi.gouv.fr/uploads/2016/01/linux_configuration-fr-v1.2.pdf
- **Détails des durcissements des sysctl sous Linux : sysctl système**: https://www.it-connect.fr/details-durcissement-sysctl-systeme-linux/
- **Ross J. Anderson Security Guide** : https://www.cl.cam.ac.uk/~rja14/

## APT Tips
<details><summary>Cliquez pour étendre</summary>

- **debsecan** : debsecan analyse la liste des paquets installés sur l'hôte actuel et signale les vulnérabilités trouvées sur le système.
- **apt-listbugs** : liste les bogues critiques avant chaque installation/mise à niveau d'APT
- **apt-listchanges** : outil qui montre ce qui a changé dans la nouvelle version d'un paquet Debian par rapport à la version actuellement installée sur le système.
- **apt-show-versions** : liste les versions disponibles des paquets et leur distribution
</details>

## About Sysctl
D'après le rapport publié par l'ANSI [RECOMMANDATIONS DE CONFIGURATION D'UN SYSTÈME GNU/LINUX](https://www.ssi.gouv.fr/uploads/2016/01/linux_configuration-fr-v1.2.pdf)

Les sysctl sont un ensemble de variables qui permettent d’adapter le paramétrage du système d’exploitation et plus particulièrement de son noyau.

Cet ensemble est enrichi au gré des évolutions et des améliorations qui sont apportées au système.

Leurs portée, usage et paramétrage doivent donc faire l’objet d’une veille régulière si un administrateur souhaite profiter au mieux de leurs fonctionnalités.
Les sysctl peuvent agir à tout niveau :
- **mémoire** : configuration de la pagination, des allocateurs, des propriétés des mappings, etc.
- **réseau** : IP, TCP, UDP, tailles et caractéristiques des tampons, fonctions évoluées (syn cookies,
choix d’algorithmes, etc.) ;
- **noyau** : contrôle des caches, swap, scheduling, etc.
- **systèmes de fichiers** : setuid dumps, hard et soft links, quotas ;
- **processus** : ressources allouées, restrictions d’exécution, cloisonnement, etc.
### Example de configuration
#### Listing 6.1 – Paramétrage des sysctl réseau d’un « serveur »
Les sysctl détaillées dans cet exemple sont recommandées pour un hôte de type « **serveur** »
n’effectuant pas de routage et ayant une configuration IPv6 minimaliste. Elles sont présentées telles que rencontrées dans le fichier ```/etc/sysctl.conf```
### Installation

Pour appliquer une [configuration sysctl basé sur les recommandations evoqué ci-avant dans le rapport de l'ANSSI](/configs/sysctl.conf). Executer le script d'installation comme ceci :
```bash
./install.sh
```
# Security tools
## **Lynis** 
Pour analyser et effectuer des opérations d'audit sur votre système, Lynis auditing tool s'avère très efficace pour dresser un bilan de santé de votre système.

![Lynis-img](docs/lynis-img.png)
### Installation
- Debian
```bash
sudo apt install lynis
```
- Red Hat
```bash
sudo yum install lynis
```
### Installation via Git
```bash
cd /usr/local
```
```bash
git clone https://github.com/CISOfy/lynis
```
```bash
sudo ./lynis
```
### Utilisation
- Lancer lynis (effectuer une analyse de sécurité locale)
```bash
sudo lynis audit system
```
## **Rkhunter**
Pour vous protéger des éventuels rootkits, portes dérobées et exploits, rkhunter se révèle être un outil intéressant.

![Lynis-img](docs/rkhunter-overview.png)
### Installation
- Debian
```bash
sudo apt install rkhunter
```
### Utilisation
- Lancer rkhunter (Vérifier le système local)
```bash
sudo rkhunter --check
```
### Configuration
Pour configurer rkhunter, éditez le fichier ```/etc/rkhunter.conf```

Pour plus d'informations, consultez le site :
https://www.vultr.com/docs/how-to-install-rkhunter-on-debian-10/

## **USBGUARD** 
USBGuard aide à protéger votre ordinateur contre les périphériques USB malveillants (alias BadUSB) en mettant en œuvre des capacités de base de liste blanche et de liste noire basées sur les attributs du périphérique.
Il utilise une infrastructure de blocage de périphériques incluse dans le noyau Linux et se compose d'un démon et de quelques interfaces.

### Installation
- Debian
```bash
sudo apt install usbguard
```
### Utilisation
- Pour vérifier que notre système détecte la présence de notre clé USB
```bash
lsusb
```
- Générer la politique par défaut d’usbguard
```bash
sudo usbguard generate-policy > /etc/usbguard/rules.conf
```
- Redemarrer et activer le service au démarrage
```bash
sudo systemctl restart usbguard.service
sudo systemctl enable usbguard.service
```
- Lister tous les périphériques USB reconnus par le démon USBGuard.
```bash
sudo usbguard list-devices
```
- Autoriser un périphérique
```bash
sudo usbguard allow-device <id_du_périphérique>
```
- Bloquer un périphérique
```bash
sudo usbguard block-device <id_du_périphérique>
```
- Autoriser de manière permanente un périphérique externe

1. Obtenir la ligne concernant le périphérique avec la commande ci-dessous :
```bash
sudo usbguard list-devices
```

2. Collez la ligne à la fin du fichier
```bash
sudo nano /etc/usbguard/rules.conf
```
