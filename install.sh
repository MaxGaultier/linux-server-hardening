#!/bin/bash
## check root
if [ "$EUID" -ne 0 ]
  then echo "Please run the script with sudo"
  exit
fi
printf "\n== Running installation...\n"
# Update
printf "\n== Backup sysctl default configs...\n"
apt update -y

# Backup default configs
printf "\n== Backup sysctl default configs...\n"
unlink /etc/sysctl.conf /etc/sysctl.d/
mv /etc/sysctl.conf /etc/sysctl.conf.BACK
mv /etc/sysctl.d/99-sysctl.conf /etc/sysctl.d/99-sysctl.conf.BACK

# Installation
cp ./configs/sysctl.conf /etc/sysctl.conf
ln -s /etc/sysctl.conf /etc/sysctl.d/99-sysctl.conf

# Update sysctl conf
sysctl -p

# End
printf "\n== Successful installation...\n"
